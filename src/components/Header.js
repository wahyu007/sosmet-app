import { Navbar } from 'react-bootstrap'

function Header() {
  return (
   <Navbar bg='light'>
     <Navbar.Brand>Sosmet</Navbar.Brand>
   </Navbar>
  );
}

export default Header;
