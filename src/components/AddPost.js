import { Modal, Button, Form, Row, Col, Image } from 'react-bootstrap';
import React from 'react';
import userIcon from '../assets/user-icon-2.png';

function AddPost({show, handleClose}) {
    return (
        <Modal show={show} onHide={handleClose} size="lg">
            <Modal.Body>
                <Row>
                    <Col sm={1}><Image width="50px" height="50px" src={userIcon} rounded /></Col>
                    <Col sm={11}>
                        <Form.Group controlId="exampleForm.ControlTextarea1">
                            <Form.Control as="textarea" rows={3} placeholder="What happen today dear ?" />
                        </Form.Group>
                    </Col>
                </Row>
                <div className="d-flex flex-row-reverse">
                    <Button className="w-25" variant="outline-dark" size="sm" onClick={handleClose}>
                        POST
                    </Button>
                </div>
            </Modal.Body>
            {/* <Modal.Footer>
                
            </Modal.Footer> */}
        </Modal>
    );
}

export default AddPost;