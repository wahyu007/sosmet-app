import './App.css';
import React, { Component} from 'react';
import Header from './components/Header';
import Post from './components/Post';
import AddPost from './components/AddPost';
import { Container, Row, Col, Button, Modal } from 'react-bootstrap'

class App extends Component {
  state = {
    show : false,

  }

  handleShow = () => {
    this.setState({ show : true})
  };
  handleClose = () => {
    this.setState({ show : false})
  };

  render() {
    return (
      <>
        <Header/>
        <Container>
          <Row>
            <Col sm={8}>
              <Row>
                <Col sm={10}>
                  <h3>Latest Post</h3>
                </Col>
                <Col sm={2}>
                  <Button onClick={this.handleShow} variant="outline-dark" size="sm" block>Post</Button>
                </Col>
              </Row>
              <br></br>
              <Post />
              <Post />
              <Post />
            </Col>
            <Col sm={4}>col 4</Col>
          </Row>
        </Container>
        <AddPost show={this.state.show} handleClose={this.handleClose} />
      </>
    )
  }
}

export default App;